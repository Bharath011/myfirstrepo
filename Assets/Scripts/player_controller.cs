﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class player_controller : MonoBehaviour
{
	public float speed;
	private Animator animator;

	// Use this for initialization
	void Start()
	{
		animator = this.GetComponent<Animator>();
	}

	// Update is called once per frame
	void Update()
	{
		Vector2 moveDirection = Vector2.zero;
		var vertical = Input.GetAxis("Vertical");
		var horizontal = Input.GetAxis("Horizontal");

		if (horizontal > 0)
		{
			moveDirection.x = 1;
			animator.SetInteger("Direction", 2);
		}
		else if (horizontal < 0)
		{
			moveDirection.x = -1;
			animator.SetInteger("Direction", 0);
		}
		else if (vertical > 0)
		{
			moveDirection.y = 1;
			animator.SetInteger("Direction", 1);
		}
		else if (vertical < 0)
		{
			moveDirection.y = -1;
			animator.SetInteger("Direction", 3);
		}
		transform.Translate(moveDirection * speed* Time.deltaTime, Space.World);
	}
}